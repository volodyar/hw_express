import { DataSource } from 'typeorm';
import { databaseConfiguration } from './data-config';

export const AppDataSource = new DataSource(databaseConfiguration());
