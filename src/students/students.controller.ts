import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as studentsService from './students.service';
import { controllerWrapper } from '../application/utils';
import {
  IStudentByNameRequest,
  IStudentCreateRequest,
  IStudentGroupUpdateRequest,
  IStudentMarksRequest,
  IStudentUpdateRequest,
} from './types';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';

async function getAll(req: IStudentByNameRequest, res: Response) {
  const { name } = req.query;
  const students = await studentsService.getAll(name);

  res.json(students);
}

async function create(
  req: ValidatedRequest<IStudentCreateRequest>,
  res: Response,
) {
  const { body } = req;
  const students = await studentsService.create(body);

  res.status(HTTP_STATUS_CODES.CREATED).json(students);
}

async function getById(req: Request, res: Response) {
  const { id } = req.params;
  const student = await studentsService.getById(id);

  res.json(student);
}

async function getMarksById(req: IStudentMarksRequest, res: Response) {
  const { student } = req.query;
  const result = await studentsService.getMarksById(student);

  res.json(result);
}

async function updateById(
  req: ValidatedRequest<IStudentUpdateRequest>,
  res: Response,
) {
  const { id } = req.params;
  const { body } = req;
  await studentsService.updateById(id, body);

  res.status(HTTP_STATUS_CODES.NO_CONTENT).end();
}

async function updateGroupById(
  req: ValidatedRequest<IStudentGroupUpdateRequest>,
  res: Response,
) {
  const { student } = req.query;
  const { body } = req;
  await studentsService.updateGroupById(student, body);

  res.status(HTTP_STATUS_CODES.NO_CONTENT).end();
}

async function removeById(req: Request, res: Response) {
  const { id } = req.params;
  await studentsService.removeById(id);

  res.status(HTTP_STATUS_CODES.NO_CONTENT).end();
}

export default {
  getAll: controllerWrapper(getAll),
  getById: controllerWrapper(getById),
  getMarksById: controllerWrapper(getMarksById),
  create: controllerWrapper(create),
  updateById: controllerWrapper(updateById),
  updateGroupById: controllerWrapper(updateGroupById),
  removeById: controllerWrapper(removeById),
};
