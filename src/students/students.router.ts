import { Router } from 'express';
import studentsController from './students.controller';
import { validate } from './../application/middlewares';
import studentsSchema from './students.schema';
import idSchema from './../application/schemas/schemas';

const router = Router();

router.get('/', studentsController.getAll);

router.get('/marks', studentsController.getMarksById);

router.patch(
  '/group',
  validate.body(studentsSchema.updateStudentsGroupSchema),
  studentsController.updateGroupById,
);

router.get('/:id', validate.params(idSchema), studentsController.getById);

router.post(
  '/',
  validate.body(studentsSchema.createStudentsSchema),
  studentsController.create,
);

router.patch(
  '/:id',
  validate.params(idSchema),
  validate.body(studentsSchema.updateStudentsSchema),
  studentsController.updateById,
);

router.delete('/:id', validate.params(idSchema), studentsController.removeById);

export default router;
