import HttpException from './../application/exception/HttpException';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { IStudent } from './types';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/students.entity';
import { Mark } from '../marks/entities/mark.entity';

const studentsRepository = AppDataSource.getRepository(Student);
const marksRepository = AppDataSource.getRepository(Mark);

export async function getAll(name: string | undefined): Promise<Student[]> {
  return await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.age as age',
      'student.created_at as created_at',
      'student.updated_at as updated_at',
      'student.email as email',
      'student.group_id as group_id',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where(`${!name}`)
    .orWhere('student.name ilike :name', { name })
    .orderBy('student.id')
    .getRawMany();
}

export async function getById(id: string): Promise<IStudent> {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.age as age',
      'student.created_at as created_at',
      'student.updated_at as updated_at',
      'student.email as email',
      'student.group_id as group_id',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "group_name"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Student not found');

  return student;
}

export async function getMarksById(id: string): Promise<Student> {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select(['student', 'marks.mark', 'course.name'])
    .leftJoin('student.marks', 'marks')
    .leftJoin('marks.course', 'course')
    .where('student.id = :id', { id })
    .getOne();

  if (!student)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Student not found');

  return student;
}

export async function create(body: Omit<IStudent, 'id'>): Promise<Student> {
  const student = await studentsRepository.findOneBy({ email: body.email });

  if (student)
    throw new HttpException(HTTP_STATUS_CODES.BAD_REQUEST, 'Email in use!');

  return await studentsRepository.save(body);
}

export async function updateById(
  id: string,
  studentProps: Partial<IStudent>,
): Promise<void> {
  const result = await studentsRepository.update(id, studentProps);

  if (!result.affected)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Student not found');
}

export async function updateGroupById(
  id: string,
  studentProps: Pick<IStudent, 'groupId'>,
): Promise<void> {
  const result = await studentsRepository.update(id, studentProps);

  if (!result.affected)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Student not found');
}

export async function removeById(id: string): Promise<void> {
  await marksRepository
    .createQueryBuilder('marks')
    .delete()
    .where('marks.student_id = :id', { id })
    .execute();

  const result = await studentsRepository.delete(id);

  if (!result.affected)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Student not found');
}
