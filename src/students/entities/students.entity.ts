import {
  Entity,
  Column,
  ManyToOne,
  JoinColumn,
  OneToMany,
  Index,
} from 'typeorm';
import { CommonEntity } from '../../application/entity/Common.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CommonEntity {
  @Index()
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  surname: string;

  @Column({ type: 'varchar' })
  email: string;

  @Column({ type: 'integer' })
  age: number;

  @Column({ type: 'varchar', name: 'image_path', nullable: true })
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({ name: 'group_id', nullable: true, type: 'int' })
  groupId: number | null;

  @OneToMany(() => Mark, (mark) => mark.student)
  marks: Mark[];
}
