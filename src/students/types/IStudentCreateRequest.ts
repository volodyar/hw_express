import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IStudent } from './index';

export interface IStudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IStudent, 'id'>;
}
