import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IStudent } from './index';

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>;
}
