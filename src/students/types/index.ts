export { IStudent } from './IStudent';
export { IStudentCreateRequest } from './IStudentCreateRequest';
export { IStudentGroupUpdateRequest } from './IStudentGroupUpdateRequest';
export { IStudentUpdateRequest } from './IStudentUpdateRequest';
export { IStudentMarksRequest } from './IStudentMarksRequest';
export { IStudentByNameRequest } from './IStudentByNameRequest';
