import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';

export interface IStudentMarksRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: { student: string };
}
