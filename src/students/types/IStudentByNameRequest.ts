import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IStudent } from './index';

export interface IStudentByNameRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: Pick<IStudent, 'name'>;
}
