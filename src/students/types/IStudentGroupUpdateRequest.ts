import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IStudent } from './index';

export interface IStudentGroupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<IStudent, 'groupId'>;
}
