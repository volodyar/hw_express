import Joi from 'joi';
import { IStudent } from './types';

const createStudentsSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().required(),
  surname: Joi.string().required(),
  email: Joi.string().email().required(),
  age: Joi.number().min(17).required(),
  imagePath: Joi.string().default(null),
  groupId: Joi.number().default(null),
});

const updateStudentsSchema = Joi.object<Partial<IStudent>>({
  name: Joi.string(),
  surname: Joi.string(),
  email: Joi.string().email(),
  age: Joi.number().min(17),
  imagePath: Joi.string(),
  groupId: Joi.number(),
});

const updateStudentsGroupSchema = Joi.object<Pick<IStudent, 'groupId'>>({
  groupId: Joi.number(),
});

export default {
  updateStudentsSchema,
  createStudentsSchema,
  updateStudentsGroupSchema,
};
