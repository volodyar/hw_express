import Joi from 'joi';
import { IGroup } from './types';

const createGroupsSchema = Joi.object<Omit<IGroup, 'id'>>({
  name: Joi.string().required(),
});

const updateGroupsSchema = Joi.object<Partial<IGroup>>({
  name: Joi.string().required(),
});

export default {
  updateGroupsSchema,
  createGroupsSchema,
};
