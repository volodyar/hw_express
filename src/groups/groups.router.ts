import { Router } from 'express';
import groupsController from './groups.controller';
import { validate } from '../application/middlewares';
import groupsSchema from './groups.schema';
import idSchema from '../application/schemas/schemas';

const router = Router();

router.get('/', groupsController.getAll);

router.post(
  '/',
  validate.body(groupsSchema.createGroupsSchema),
  groupsController.create,
);

router.get('/:id', validate.params(idSchema), groupsController.getById);

router.patch(
  '/:id',
  validate.params(idSchema),
  validate.body(groupsSchema.updateGroupsSchema),
  groupsController.updateById,
);

router.delete('/:id', groupsController.removeById);

export default router;
