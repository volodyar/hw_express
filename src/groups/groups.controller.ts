import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as groupsService from './groups.service';
import { controllerWrapper } from '../application/utils';
import { IGroupCreateRequest, IGroupUpdateRequest } from './types';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';

async function getAll(req: Request, res: Response) {
  const groups = await groupsService.getAll();

  res.json(groups);
}

async function create(
  req: ValidatedRequest<IGroupCreateRequest>,
  res: Response,
) {
  const { body } = req;
  const groups = await groupsService.create(body);

  res.status(HTTP_STATUS_CODES.CREATED).json(groups);
}

async function getById(req: Request, res: Response) {
  const { id } = req.params;
  const group = await groupsService.getById(id);

  res.json(group);
}

async function updateById(
  req: ValidatedRequest<IGroupUpdateRequest>,
  res: Response,
) {
  const { id } = req.params;
  const { body } = req;
  await groupsService.updateById(id, body);

  res.status(HTTP_STATUS_CODES.NO_CONTENT).end();
}

async function removeById(req: Request, res: Response) {
  const { id } = req.params;
  await groupsService.removeById(id);

  res.status(HTTP_STATUS_CODES.NO_CONTENT).end();
}

export default {
  getAll: controllerWrapper(getAll),
  getById: controllerWrapper(getById),
  create: controllerWrapper(create),
  removeById: controllerWrapper(removeById),
  updateById: controllerWrapper(updateById),
};
