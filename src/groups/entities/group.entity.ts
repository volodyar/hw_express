import { Entity, Column, OneToMany } from 'typeorm';
import { CommonEntity } from '../../application/entity/Common.entity';
import { Student } from '../../students/entities/students.entity';

@Entity({ name: 'groups' })
export class Group extends CommonEntity {
  @Column({ type: 'varchar' })
  name: string;

  @OneToMany(() => Student, (student) => student.group)
  students: Student[];
}
