import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IGroup } from './index';

export interface IGroupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IGroup>;
}
