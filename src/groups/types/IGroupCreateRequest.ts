import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IGroup } from './index';

export interface IGroupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IGroup, 'id'>;
}
