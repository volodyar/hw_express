import HttpException from '../application/exception/HttpException';
import { IGroup } from './types';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import { Student } from '../students/entities/students.entity';

const groupsRepository = AppDataSource.getRepository(Group);
const studentsRepository = AppDataSource.getRepository(Student);

export async function getAll(): Promise<IGroup[]> {
  return await groupsRepository
    .createQueryBuilder('groups')
    .leftJoinAndSelect('groups.students', 'students')
    .getMany();
}

export async function getById(id: string): Promise<IGroup> {
  const group = await groupsRepository
    .createQueryBuilder('groups')
    .leftJoinAndSelect('groups.students', 'students')
    .where('groups.id = :id', { id })
    .getOne();

  if (!group)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'The group not found');

  return group;
}

export async function create(body: Omit<IGroup, 'id'>): Promise<IGroup> {
  const group = await groupsRepository.findOne({ where: { name: body.name } });

  if (group)
    throw new HttpException(
      HTTP_STATUS_CODES.BAD_REQUEST,
      'The group with the such name already exist',
    );

  return await groupsRepository.save(body);
}

export async function updateById(
  id: string,
  studentProps: Partial<IGroup>,
): Promise<void> {
  const result = await groupsRepository.update(id, studentProps);

  if (!result.affected)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'The group not found');
}

export async function removeById(id: string): Promise<void> {
  await studentsRepository
    .createQueryBuilder('students')
    .update(Student)
    .set({ groupId: null })
    .where('students.group_id = :id', { id })
    .execute();

  const result = await groupsRepository.delete(id);

  if (!result.affected)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'The group not found');
}
