import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IMark } from './index';

export interface IMarkCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IMark, 'id'>;
}
