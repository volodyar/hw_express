import { Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as coursesService from './marks.service';
import { controllerWrapper } from '../application/utils';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { IMarkCreateRequest } from './types';

async function create(
  req: ValidatedRequest<IMarkCreateRequest>,
  res: Response,
) {
  const { body } = req;
  const result = await coursesService.create(body);

  res.status(HTTP_STATUS_CODES.CREATED).json(result);
}

export default {
  create: controllerWrapper(create),
};
