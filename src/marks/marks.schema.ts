import Joi from 'joi';
import { IMark } from './types';

const createMarkSchema = Joi.object<Omit<IMark, 'id'>>({
  mark: Joi.string().min(0).max(100).required(),
  courseId: Joi.number().required(),
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
});

export default {
  createMarkSchema,
};
