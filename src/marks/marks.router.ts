import { Router } from 'express';
import marksController from './marks.controller';
import { validate } from '../application/middlewares';
import markSchema from './marks.schema';

const router = Router();

router.post(
  '/',
  validate.body(markSchema.createMarkSchema),
  marksController.create,
);

export default router;
