import HttpException from '../application/exception/HttpException';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { AppDataSource } from '../configs/database/data-source';
import { IMark } from './types';
import { Mark } from './entities/mark.entity';
import { LectorCourse } from '../application/entity/lector_course.entity';

const marksRepository = AppDataSource.getRepository(Mark);
const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);

export async function create(body: Omit<IMark, 'id'>): Promise<Mark> {
  const [lectorCourse] = await lectorCourseRepository.find({
    where: { courseId: body.courseId, lectorId: body.lectorId },
  });

  if (!lectorCourse) {
    throw new HttpException(
      HTTP_STATUS_CODES.BAD_REQUEST,
      'Lector is not present in the course',
    );
  }

  const mark = await marksRepository.findOne({
    where: { ...body },
  });

  if (mark)
    throw new HttpException(
      HTTP_STATUS_CODES.BAD_REQUEST,
      'The mark is already set',
    );

  return await marksRepository.save(body);
}
