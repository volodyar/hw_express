import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';
import { CommonEntity } from '../../application/entity/Common.entity';
import { Course } from '../../courses/entities/course.entity';
import { Student } from '../../students/entities/students.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'marks' })
export class Mark extends CommonEntity {
  @Column({ type: 'int' })
  mark: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @Column({ name: 'course_id', nullable: true, type: 'int' })
  courseId: number;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @Column({ name: 'student_id', nullable: true, type: 'int' })
  studentId: number;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @Column({ name: 'lector_id', nullable: true, type: 'int' })
  lectorId: number;
}
