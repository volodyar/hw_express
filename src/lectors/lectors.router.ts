import { Router } from 'express';
import lectorsController from './lectors.controller';
import { validate } from '../application/middlewares';
import lectorSchema from './lectors.schema';

const router = Router();

router.get('/', lectorsController.getAll);

router.get('/courses', lectorsController.getCoursesById);

router.get('/students', lectorsController.getStudentsById);

router.post(
  '/',
  validate.body(lectorSchema.createLectorSchema),
  lectorsController.create,
);

export default router;
