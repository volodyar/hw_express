import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as lectorsService from './lectors.service';
import { controllerWrapper } from '../application/utils';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { ILectorFilterByIdRequest, ILectorCreateRequest } from './types';

async function getAll(req: Request, res: Response) {
  const lectors = await lectorsService.getAll();
  res.json(lectors);
}

async function create(
  req: ValidatedRequest<ILectorCreateRequest>,
  res: Response,
) {
  const { body } = req;
  const lector = await lectorsService.create(body);

  res.status(HTTP_STATUS_CODES.CREATED).json(lector);
}

async function getCoursesById(req: ILectorFilterByIdRequest, res: Response) {
  const { lector } = req.query;
  const result = await lectorsService.getCoursesById(lector);

  res.json(result);
}

async function getStudentsById(req: ILectorFilterByIdRequest, res: Response) {
  const { lector } = req.query;
  const result = await lectorsService.getStudentsById(lector);

  res.json(result);
}

export default {
  getAll: controllerWrapper(getAll),
  getCoursesById: controllerWrapper(getCoursesById),
  create: controllerWrapper(create),
  getStudentsById: controllerWrapper(getStudentsById),
};
