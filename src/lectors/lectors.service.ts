import HttpException from '../application/exception/HttpException';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from './entities/lector.entity';
import { ILector } from './types/ILector';
import { Student } from '../students/entities/students.entity';
import uniqby from 'lodash.uniqby';
import { ILectorWithStudents } from './types';

const lectorsRepository = AppDataSource.getRepository(Lector);

export async function getAll(): Promise<Lector[]> {
  return await lectorsRepository.find();
}

export async function getCoursesById(id: string): Promise<Lector> {
  const lector = await lectorsRepository
    .createQueryBuilder('lectors')
    .select(['lectors'])
    .addSelect('lector_course.id')
    .addSelect([
      'course.id',
      'course.name',
      'course.description',
      'course.hours',
    ])
    .leftJoin('lectors.courses', 'lector_course')
    .leftJoin('lector_course.course', 'course')
    .where('lectors.id = :id', { id })
    .getOne();

  if (!lector)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Lector not found');

  return lector;
}

export async function getStudentsById(
  id: string,
): Promise<ILectorWithStudents> {
  const lector = await lectorsRepository
    .createQueryBuilder('lectors')
    .select(['lectors', 'marks'])
    .addSelect([
      'students.id',
      'students.name',
      'students.surname',
      'students.email',
      'students.age',
      'students.imagePath',
      'students.groupId',
    ])
    .leftJoin('lectors.marks', 'marks')
    .leftJoin('marks.student', 'students')
    .where('lectors.id = :id', { id })
    .getOne();

  if (!lector)
    throw new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Lector not found');

  const students: Student[] = uniqby(
    lector.marks.map((mark) => mark.student),
    'id',
  );
  const newLector: ILectorWithStudents = {
    id: Number(lector.id),
    name: lector.name,
    email: lector.email,
    password: Number(lector.password),
    createdAt: lector.createdAt,
    updatedAt: lector.updatedAt,
    students,
  };

  return newLector as ILectorWithStudents;
}

export async function create(body: Omit<ILector, 'id'>): Promise<Lector> {
  const lector = await lectorsRepository.findOne({
    where: { email: body.email },
  });

  if (lector)
    throw new HttpException(HTTP_STATUS_CODES.BAD_REQUEST, 'Email in use!');

  return await lectorsRepository.save(body);
}
