import { Entity, Column, OneToMany } from 'typeorm';
import { CommonEntity } from '../../application/entity/Common.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { LectorCourse } from '../../application/entity/lector_course.entity';

@Entity({ name: 'lectors' })
export class Lector extends CommonEntity {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'varchar' })
  email: string;

  @Column({ type: 'integer' })
  password: number;

  @OneToMany(() => LectorCourse, (lectorCourse) => lectorCourse.lector)
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector)
  marks: Mark[];
}
