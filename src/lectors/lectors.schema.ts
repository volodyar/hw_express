import Joi from 'joi';
import { ILector } from './types';

const createLectorSchema = Joi.object<Omit<ILector, 'id'>>({
  name: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.number().required(),
});

export default {
  createLectorSchema,
};
