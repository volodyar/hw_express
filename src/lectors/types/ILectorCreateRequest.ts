import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { ILector } from './index';

export interface ILectorCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ILector, 'id'>;
}
