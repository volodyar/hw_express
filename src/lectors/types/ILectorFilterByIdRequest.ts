import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';

export interface ILectorFilterByIdRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: { lector: string };
}
