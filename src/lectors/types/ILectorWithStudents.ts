import { Student } from '../../students/entities/students.entity';
import { ILector } from './ILector';

export interface ILectorWithStudents extends ILector {
  updatedAt: Date;
  createdAt: Date;
  students: Student[];
}
