export { ILector } from './ILector';
export { ILectorCreateRequest } from './ILectorCreateRequest';
export { ILectorWithStudents } from './ILectorWithStudents';
export { ILectorFilterByIdRequest } from './ILectorFilterByIdRequest';
