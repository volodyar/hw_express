import HttpException from '../application/exception/HttpException';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from './entities/course.entity';
import { ICourse, ICourseMarks, ILectorCourse } from './types';
import { LectorCourse } from '../application/entity/lector_course.entity';

const coursesRepository = AppDataSource.getRepository(Course);
const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);

export async function getAll(): Promise<Course[]> {
  return await coursesRepository.find();
}

export async function create(body: Omit<ICourse, 'id'>): Promise<Course> {
  const course = await coursesRepository.findOne({
    where: { name: body.name },
  });

  if (course)
    throw new HttpException(
      HTTP_STATUS_CODES.BAD_REQUEST,
      'The course already exist!',
    );

  return await coursesRepository.save(body);
}

export async function getMarksById(id: string): Promise<ICourseMarks[]> {
  const course = await coursesRepository.findOneBy({ id });

  if (!course)
    throw new HttpException(
      HTTP_STATUS_CODES.NOT_FOUND,
      'The course not found',
    );

  const marks = await coursesRepository
    .createQueryBuilder('courses')
    .select([
      'courses.name as course_name',
      'lector.name as lector_name',
      'student.name as student_name',
      'marks.mark as mark',
    ])
    .innerJoin('courses.marks', 'marks')
    .innerJoin('marks.lector', 'lector')
    .innerJoin('marks.student', 'student')
    .where('courses.id = :id', { id })
    .getRawMany();

  return marks;
}

export async function addLectorById(body: ILectorCourse): Promise<void> {
  const result = await lectorCourseRepository.save(body);

  if (!result)
    throw new HttpException(
      HTTP_STATUS_CODES.NOT_FOUND,
      'Something went wrong!',
    );
}
