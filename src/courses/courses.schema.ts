import Joi from 'joi';
import { ICourse } from './types';

const createCourseSchema = Joi.object<Omit<ICourse, 'id'>>({
  name: Joi.string().required(),
  description: Joi.string().required(),
  hours: Joi.number().required(),
});

export default {
  createCourseSchema,
};
