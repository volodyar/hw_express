import { Entity, Column, OneToMany } from 'typeorm';
import { CommonEntity } from '../../application/entity/Common.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { LectorCourse } from '../../application/entity/lector_course.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'courses' })
export class Course extends CommonEntity {
  @Column({ type: 'varchar', unique: true })
  name: string;

  @Column({ type: 'varchar' })
  description: string;

  @Column({ type: 'integer' })
  hours: number;

  @OneToMany(() => LectorCourse, (lectorCourse) => lectorCourse.course)
  lectors: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
