import { Router } from 'express';
import coursesController from './courses.controller';
import { validate } from '../application/middlewares';
import courseSchema from './courses.schema';

const router = Router();

router.get('/', coursesController.getAll);
router.post('/:courseId', coursesController.addLectorById);
router.get('/marks', coursesController.getMarksById);

router.post(
  '/',
  validate.body(courseSchema.createCourseSchema),
  coursesController.create,
);

export default router;
