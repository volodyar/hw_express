import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { LectorCourse } from '../../application/entity/lector_course.entity';

export interface ICourseAddLectorRequest extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Pick<LectorCourse, 'courseId'>;
  [ContainerTypes.Body]: Pick<LectorCourse, 'lectorId'>;
}
