export interface ICourseMarks {
  courseName: string;
  lectorName: string;
  studentName: string;
  mark: number;
}
