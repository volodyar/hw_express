import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { ICourse } from './index';

export interface ICourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ICourse, 'id'>;
}
