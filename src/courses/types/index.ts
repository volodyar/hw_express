export { ICourse } from './ICourse';
export { ILectorCourse } from './ILectorCourse ';
export { ICourseCreateRequest } from './ICourseCreateRequest';
export { ICourseMarks } from './ICourseMarks';
export { ICourseByIdRequest } from './ICourseByIdRequest';
export { ICourseAddLectorRequest } from './ICourseAddLectorRequest';
