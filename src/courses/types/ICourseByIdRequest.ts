import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';

export interface ICourseByIdRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: { course: string };
}
