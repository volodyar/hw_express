import { Request, Response } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as coursesService from './courses.service';
import { controllerWrapper } from '../application/utils';
import { HTTP_STATUS_CODES } from '../application/enums/httpStatusCode';
import {
  ICourseAddLectorRequest,
  ICourseByIdRequest,
  ICourseCreateRequest,
} from './types';

async function getAll(req: Request, res: Response) {
  const courses = await coursesService.getAll();
  res.json(courses);
}

async function create(
  req: ValidatedRequest<ICourseCreateRequest>,
  res: Response,
) {
  const { body } = req;
  const course = await coursesService.create(body);

  res.status(HTTP_STATUS_CODES.CREATED).json(course);
}

async function getMarksById(req: ICourseByIdRequest, res: Response) {
  const { course } = req.query;
  const marks = await coursesService.getMarksById(course);

  res.json(marks);
}

async function addLectorById(req: ICourseAddLectorRequest, res: Response) {
  const { courseId } = req.params;
  const { lectorId } = req.body;

  await coursesService.addLectorById({
    courseId,
    lectorId,
  });

  res.status(HTTP_STATUS_CODES.NO_CONTENT).end();
}

export default {
  getAll: controllerWrapper(getAll),
  addLectorById: controllerWrapper(addLectorById),
  create: controllerWrapper(create),
  getMarksById: controllerWrapper(getMarksById),
};
