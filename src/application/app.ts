import express from 'express';
import cors from 'cors';
import path from 'path';
import studentsRouter from './../students/students.router';
import groupsRouter from './../groups/groups.router';
import coursesRouter from './../courses/courses.router';
import lectorsRouter from './../lectors/lectors.router';
import marksRouter from './../marks/marks.router';
import exceptionFilter from './exception/exceptionFilter';
import { HTTP_STATUS_CODES } from './enums/httpStatusCode';
import HttpException from './exception/HttpException';
import { AppDataSource } from '../configs/database/data-source';

const app = express();

app.use(express.json());
app.use(cors());
AppDataSource.initialize()
  .then(() => console.log('Connected succesfully'))
  .catch((e) => console.log(e));

const imageDestination = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(imageDestination));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', coursesRouter);
app.use('/api/v1/lectors', lectorsRouter);
app.use('/api/v1/marks', marksRouter);

app.use((req, res, next) => {
  next(new HttpException(HTTP_STATUS_CODES.NOT_FOUND, 'Not found'));
});

app.use('/', exceptionFilter);

export default app;
