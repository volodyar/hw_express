import multer from 'multer';
import path from 'path';

const multerDestination = path.join(__dirname, '../../', 'temp');

const storage = multer.diskStorage({
  destination: multerDestination,
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

export const updaloadMiddleware = multer({ storage: storage });
