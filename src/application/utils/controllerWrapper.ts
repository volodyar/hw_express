import { NextFunction, Request, Response } from 'express';

export function controllerWrapper(func: any) {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      await func(req, res);
    } catch (err) {
      next(err);
    }
  };
}
