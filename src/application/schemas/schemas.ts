import Joi from 'joi';

const id = Joi.object<{ id: string }>({
  id: Joi.string().required(),
});

export default id;
