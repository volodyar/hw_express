import { Column, Entity, JoinColumn, ManyToOne, Unique } from 'typeorm';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { CommonEntity } from './Common.entity';

@Entity({ name: 'lector_course' })
@Unique(['lectorId', 'courseId'])
export class LectorCourse extends CommonEntity {
  @Column({ type: 'int', name: 'lector_id' })
  lectorId: number;

  @Column({ type: 'int', name: 'course_id' })
  courseId: number;

  @ManyToOne(() => Course, (course) => course.lectors, {
    nullable: false,
    eager: false,
  })
  @JoinColumn([{ name: 'course_id' }])
  course: Course;

  @ManyToOne(() => Lector, (lector) => lector.courses, {
    nullable: false,
    eager: false,
  })
  @JoinColumn([{ name: 'lector_id' }])
  lector: Lector;
}
