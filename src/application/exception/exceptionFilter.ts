import { NextFunction, Request, Response } from 'express';
import HttpException from './HttpException';
import { HTTP_STATUS_CODES } from './../enums/httpStatusCode';

function exceptionFilter(
  error: HttpException,
  req: Request,
  res: Response,
  next: NextFunction,
): void {
  const status = error.status || HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Internal server error';

  res.status(status).json({ status, message });
}

export default exceptionFilter;
