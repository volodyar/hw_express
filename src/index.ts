import 'dotenv/config';
import app from './application/app';

const { SERVER_PORT } = process.env;

app.listen(SERVER_PORT, () => {
  console.log('Server is running');
});
